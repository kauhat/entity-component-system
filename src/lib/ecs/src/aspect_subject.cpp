#include "ecs/aspect_subject.h"
#include "ecs/world.h"
#include "ecs/entity.h"
#include "ecs/component.h"

#include <algorithm>
#include <iostream>

namespace ECS
{
    void AspectSubject::changed(Entity_Shared entity)
    {
        for(auto observer : subscribers)
        {
            observer->notify(entity);
        }
    }

    void AspectSubject::registerObserver(std::shared_ptr<Aspect> aspect)
    {
        if(aspect)
        {
            subscribers.push_back(aspect);

            // Todo: Brute forced hack.
            auto world = world_.lock();
            if(world)
            {
                // Get all entities.
                auto entities = world->entity_manager.get();
                for(Entity_Shared entity : entities)
                {
                    aspect->notify(entity);
                }
            }
        }
    }

    void AspectSubject::unregisterObserver(std::shared_ptr<Aspect> aspect)
    {
        auto it = std::find(subscribers.begin(), subscribers.end(), aspect);
        if(it != subscribers.end())
        {
            subscribers.erase(it);
        }
    }
}