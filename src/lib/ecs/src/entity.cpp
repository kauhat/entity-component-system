#include "ecs/entity.h"

using namespace Core;

namespace ECS
{
    EntityBuilder& EntityBuilder::id(ID id)
    {
        id_ = id;
        return *this;
    }

    EntityBuilder& EntityBuilder::world(World_Shared world)
    {
        world_ = world;
        return *this;
    }

    Entity_Shared EntityBuilder::build() const
    {
        Entity_Shared entity = std::make_shared<Entity>();
        entity->id = id_;
        entity->world = world_;

        return entity;
    }
}