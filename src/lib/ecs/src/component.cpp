#include "ecs/component.h"
#include "ecs/component_type.h"

using namespace Core;

namespace ECS
{
    /*
     * Component
     */
    Component::~Component()
    {}

	bool Component::isType(ComponentType_Shared const& type)
	{
		return (getType() == type);
	}

	ComponentType_Shared Component::getType()
	{
		return type.lock();
	}

    /*
     * Component Builder
     */

    ComponentBuilder& ComponentBuilder::id(ID id)
    {
        id_ = id;
        return *this;
    }

    ComponentBuilder& ComponentBuilder::world(World_Shared const& world)
    {
        world_ = world;
        return *this;
    }

    ComponentBuilder& ComponentBuilder::entity(Entity_Shared const& entity)
    {
        entity_ = entity;
        return *this;
    }

    ComponentBuilder& ComponentBuilder::type(ComponentType_Shared const& type)
    {
		if (!type)
		{
			throw std::bad_exception();
		}

        type_ = type;
        return *this;
    }

    Component_Shared ComponentBuilder::build() const
    {
        Component_Shared component = type_->factory();
        component->id       = id_;
        component->world    = world_;
        component->entity   = entity_;
        component->type     = type_;

        return component;
    }
}