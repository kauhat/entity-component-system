#include "ecs/manager.h"

namespace ECS
{
    World_Shared Manager::getWorld() const
    {
        return world_.lock();
    }

    void Manager::setWorld(World_Shared const& world)
    {
        world_ = world;
    }
}