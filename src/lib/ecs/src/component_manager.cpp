#include "ecs/component_manager.h"
#include "ecs/world.h"

#include <core/service.h>
#include <algorithm>
#include <iterator>
#include <functional>

using namespace Core;

namespace ECS
{
    /*
     * Public
     */
    
    // Create
    Component_Shared ComponentManager::create(Entity_Shared const& entity, ComponentType_Shared const& type)
    {
        if(!entity)
        {
            std::cerr
                << "Could not create component: "
                << "Entity does not exist."
                << std::endl;

            return nullptr;
        }

        auto world = world_.lock();
        if(entity->world.lock() != world)
        {
            std::cerr
                << "Could not create component: "
                << "Entity #" << entity->id << " belongs to a different world."
                << std::endl;

            return nullptr;
        }

        Component_Shared existing = get(entity, type);
        if(existing)
        {
            std::cerr
                << "Could not create component: "
                << "Component \"" << type->name << "\" "
                << "for entity #" << entity->id << " already exists."
                << std::endl;

            return existing;
        }

        // Add component to owner set.
        auto com_pair = components.emplace(ComponentBuilder()
            .world(world)
            .type(type)
            .entity(entity)
            .id(id.getNext())
            .build());

        auto com_iter = com_pair.first;

        Component_Shared component;
        component = *com_iter;

        index(component);

        // Todo: Consolidate aspect change notifications.
        world->aspect_subject.changed(entity);

        return component;
    }

    // Get
    Component_Shared ComponentManager::get(const ID component_id)
    {
        auto com_iter = components_by_id.find(component_id);
        if (com_iter != components_by_id.end())
        {
            Component_Weak      com_weak    = com_iter->second;
            Component_Shared    component   = com_weak.lock();

            if (component)
            {
                return component;
            }
        }

        // No match found.
        return nullptr;
    }

    Component_Shared ComponentManager::get(Entity_Shared const& entity, ComponentType_Shared const& type)
    {
        if(!entity)
        {
            std::cerr
                << "Could not get component by entity: "
                << "Entity doesn't exist."
                << std::endl;

            return nullptr;
        }

        if(!type)
        {
            std::cerr
                << "Could not get component by type: "
                << "Type doesn't exist."
                << std::endl;

            return nullptr;
        }

        // Ensure both entity/type sets exist.
        auto iter_by_entity = components_by_entity.find(entity);
        if (iter_by_entity == components_by_entity.end())
        {
            return nullptr;
        }

        auto iter_by_type = components_by_type.find(type);
        if (iter_by_type == components_by_type.end())
        {
            return nullptr;
        }

        // Our return value.
        Component_Shared component;

        // Get set of components for entity.
        std::set<Component_Shared> by_entity = get(entity);

        // Search set for component of type.
        auto it = std::find_if(
            by_entity.begin(), by_entity.end(),
            std::bind(&Component::isType, std::placeholders::_1, type));

        if(it != by_entity.end())
        {
            // Component exists.
            component = (*it);
        }

        return component;
    }

    std::set<Component_Shared> ComponentManager::get(Entity_Shared const& entity)
    {
        if(!entity)
        {
            std::cerr
                << "Could not get components by entity: "
                << "Entity doesn't exist."
                << std::endl;

            return std::set<Component_Shared>();
        }
        
        // Our return value.
        std::set<Component_Shared> components;
        
        // Check index map key.
        auto it = components_by_entity.find(entity);
        if (it != components_by_entity.end())
        {
            auto& by_entity = it->second;
            for(auto& weak : by_entity)
            {
                Component_Shared shared = weak.lock();

                if(shared)
                {
                    // Component exist.
                    components.insert(shared);
                }
                else
                {
                    // Remove invalid component from index.
                    by_entity.erase(weak);
                }
            }
        }

        return components;
    }

    std::set<Component_Shared> ComponentManager::get(ComponentType_Shared const& type)
    {
        if(!type)
        {
            std::cerr
                << "Could not get components by type: "
                << "Type doesn't exist."
                << std::endl;

            return std::set<Component_Shared>();
        }

        // Our return value.
        std::set<Component_Shared> components;

        // Check index map key.
        auto it = components_by_type.find(type);
        if (it != components_by_type.end())
        {
            auto& by_type = it->second;
            for(auto& weak : by_type)
            {
                Component_Shared shared = weak.lock();

                if(shared)
                {
                    // Component exist.
                    components.insert(shared);
                }
                else
                {
                    // Remove invalid component from index.
                    by_type.erase(weak);
                }
            }
        }
        
        return components;
    }

    //std::vector<Component_Shared> ComponentManager::get(Entity_Shared const& entity)
    //{
    //    // Our return value.
    //    std::vector<Component_Shared> components;
    //    
    //    // Check index map key.
    //    auto it = components_by_entity.find(entity);
    //    if (it != components_by_entity.end())
    //    {
    //        // Get internal set from index.
    //        const Set_Weak& weak_set = it->second;
    //        components.reserve(weak_set.size());
    //
    //        // Convert internal weak set to shared vector.
    //        for(Component_Weak weak_ptr : weak_set)
    //        {
    //            Component_Shared component = weak_ptr.lock();
    //
    //            if(component)
    //            {
    //                components.push_back(component);
    //            }
    //        }
    //
    //        components.shrink_to_fit();
    //    }
    //
    //    return components;
    //}

    //std::vector<Component_Shared> ComponentManager::get(ComponentType_Shared const& type)
    //{
    //    // Our return value.
    //    std::vector<Component_Shared> components;
    //    
    //    // Check index map key.
    //    auto it = components_by_type.find(type);
    //    if (it != components_by_type.end())
    //    {
    //        // Get internal set from index.
    //        const Set_Weak& weak_set = it->second;
    //        components.reserve(weak_set.size());
    //
    //        // Convert internal weak set to shared vector.
    //        for(Component_Weak weak_ptr : weak_set)
    //        {
    //            Component_Shared component = weak_ptr.lock();
    //
    //            if(component)
    //            {
    //                components.push_back(component);
    //            }
    //        }
    //
    //        components.shrink_to_fit();
    //    }
    //
    //    return components;
    //}

    // Remove
    void ComponentManager::remove(ID component_id)
    {
        Component_Shared component = get(component_id);
        remove(component);
    }

    void ComponentManager::remove(Component_Shared const& component)
    {
        if (!component)
        {
            std::cerr
                << "Could not delete component: "
                << "Component doesn't exist."
                << std::endl;

            return;
        }

        deindex(component);
        components.erase(component);

        // Todo: Consolidate aspect change notifications.
        auto ent = component->entity.lock();
        if(ent)
        {
            world_.lock()->aspect_subject.changed(ent);
        }
    }

    void ComponentManager::remove(Entity_Shared const& entity)
    {
        if (!entity)
        {
            std::cerr
                << "Could not delete components: "
                << "Entity doesn't exist."
                << std::endl;

            return;
        }

        auto components = get(entity);
        for(auto& component : components)
        {
            remove(component);
        }
    }


    size_t ComponentManager::size() const
    {
        return components.size();
    }

    /*
     * Protected
     */
    void ComponentManager::index(Component_Shared const& component)
    {
        // Add to index maps.
        components_by_id[component->id] = component;
        components_by_entity[component->entity].insert(component);
        components_by_type[component->type].insert(component);
    }

    void ComponentManager::deindex(Component_Shared const& component)
    {
        // Remove from index maps.
        components_by_id.erase(component->id);
        components_by_entity[component->entity].erase(component);
        components_by_type[component->type].erase(component);
    }

    /*
     *  Utilities
     */

    // Static
    std::set<ComponentType_Shared> ComponentManager::getTypes(std::set<Component_Shared> components)
    {
        std::set<ComponentType_Shared> types;
        for(Component_Shared component : components)
        {
            auto type = component->type.lock();
            if(type)
            {
                types.insert(type);
            }
        }

        return types;
    }
}
