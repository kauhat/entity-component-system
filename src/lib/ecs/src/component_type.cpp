#include "ecs/component_type_manager.h"

namespace ECS
{
    ComponentTypeBuilder& ComponentTypeBuilder::name(const std::string& name)
    {
        name_ = name;

        return *this;
    }

    ComponentTypeBuilder& ComponentTypeBuilder::factory(Component::CreateCallback factory)
    {
        factory_ = factory;

        return *this;
    }

    ComponentType ComponentTypeBuilder::build()  const
    {
        ComponentType type;
        type.name       = name_;
        type.factory    = factory_;

        return type;
    }
}
