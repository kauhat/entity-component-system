#include "ecs/aspect.h"
#include "ecs/world.h"
#include "ecs/entity.h"
#include "ecs/component.h"
#include "ecs/component_type.h"

#include <algorithm>
#include <iterator>

namespace ECS
{
	World_Shared Aspect::getWorld() const
	{
        return world_.lock();
	}

	void Aspect::setWorld(World_Shared const& world)
	{
		if(world)
		{
        	unregister_world();
			world_ = world;
        	register_world();
		}
	}

	AspectInterest Aspect::getInterest() const
	{
        return interest_;
	}

	void Aspect::setInterest(AspectInterest const& interest)
	{
        interest_ = interest;
        unregister_world();
        register_world();
	}

	std::set<Entity_Shared> Aspect::getEntities() const
	{
		return matched_;
	}

	bool Aspect::interested(std::set<ComponentType_Shared> types)
	{
		if(!interest_.all.empty())
		{
			std::set<ComponentType_Shared> temp;

			set_intersection(
				interest_.all.begin(), interest_.all.end(),
				types.begin(), types.end(),
				std::inserter(temp, temp.end()));

			if(temp.size() != interest_.all.size())
			{
				// Intersection is smaller then required set.
				return false;
			}
		}

		if(!interest_.any.empty())
		{
			std::set<ComponentType_Shared> temp;

			set_intersection(
				interest_.any.begin(), interest_.any.end(),
				types.begin(), types.end(),
				std::inserter(temp, temp.end()));

			if(temp.empty())
			{
				// Intersection is empty.
				return false;
			}
		}

		if(!interest_.none.empty())
		{
			std::set<ComponentType_Shared> temp;

			set_intersection(
				interest_.none.begin(), interest_.none.end(),
				types.begin(), types.end(),
				std::inserter(temp, temp.end()));

			if(!temp.empty())
			{
				// Intersection is smaller then required set.
				return false;
			}
		}

		return true;
	}

	void Aspect::notify(Entity_Shared const& entity)
	{
		auto world = world_.lock();
		auto component_types = ComponentManager::getTypes(world->component_manager.get(entity));

		bool is_interested = interested(component_types);

		auto it = matched_.find(entity);
		if(it != matched_.end())
		{
			if(!is_interested)
			{
				// No longer interested.
				matched_.erase(it);			
			}
		}
		else
		{
			if(is_interested)
			{
				// Interested.
				matched_.insert(entity);
			}
		}
	}

	// Protected functions
	void Aspect::register_world()
	{
		auto world = world_.lock();
		if(world)
		{
            std::shared_ptr<Aspect> self = this->shared_from_this();
            world->aspect_subject.registerObserver(self);
		}
	}

	void Aspect::unregister_world()
	{
		auto world = world_.lock();
		if(world)
		{
			world->aspect_subject.unregisterObserver(this->shared_from_this());
		}

		matched_.clear();
	}
}
