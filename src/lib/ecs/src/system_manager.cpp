#include "ecs/system_manager.h"
#include "ecs/world.h"

namespace ECS
{
	void SystemManager::add(System_Shared const& system)
	{
		auto world = world_.lock();
		if (!world)
		{
			std::cerr
				<< "Could not add system: "
				<< "World doesn't exist."
				<< std::endl;

			return;
		}

		systems.emplace(system);
		systems_by_type.insert(
			std::make_pair(std::type_index(typeid(system)), system));

		system->setWorld(world);
	}

	void SystemManager::process()
	{
		for(auto& sys : systems)
		{
			if(sys)
			{
				sys->process();
			}
		}
	}

	size_t SystemManager::size() const
	{
		return systems.size();
	}
}