#include "ecs/entity_system.h"

namespace ECS
{
	std::shared_ptr<Aspect> EntitySystem::getAspect()
	{
		if (aspect_ == nullptr)
		{
			// Aspect needs to be initialized.
			aspect_ = initializeAspect();
		}

		return aspect_;
	}

	std::set<Entity_Shared> EntitySystem::getEntities()
	{
		std::shared_ptr<Aspect> aspect = getAspect();
		return aspect->getEntities();
	}
}