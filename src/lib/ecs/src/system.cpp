#include "ecs/system.h"
#include "ecs/world.h"

namespace ECS
{
	System::~System()
	{}

	void System::process()
	{
		if(enabled_)
		{
			processSystem();
		}
	}

	World_Shared System::getWorld()
	{
		World_Shared world = world_.lock();
		if(world)
		{
			return world;
		}
		
		return nullptr;
	}

	void System::setWorld(World_Shared world)
	{
		if(world)
		{
			world_ = world;
		}
	}
}