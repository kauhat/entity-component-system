#include "ecs/world.h"

namespace ECS
{
    /*
     * World
     */

    /*
     * World Builder
     */
    WorldBuilder::WorldBuilder()
    : world_(std::make_shared<World>())
    {}

    World_Shared WorldBuilder::build()
    {
        world_->entity_manager.setWorld(world_);
        world_->component_manager.setWorld(world_);
        world_->component_manager.types.setWorld(world_);
        world_->system_manager.setWorld(world_);
        world_->aspect_subject.setWorld(world_);

        return world_;
    }
}