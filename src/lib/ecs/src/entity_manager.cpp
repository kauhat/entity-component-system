#include "ecs/entity_manager.h"
#include "ecs/component_manager.h"
#include "ecs/world.h"

#include <algorithm>
#include <iterator>

using namespace Core;

namespace ECS
{
    /*
     * Public
     */

    // Create
    Entity_Shared EntityManager::create()
    {
        auto ent_pair = entities.emplace(EntityBuilder()
            .id(id.getNext())
            .world(world_.lock())
            .build());

        auto ent_iter = ent_pair.first;
        Entity_Shared entity = *ent_iter;

        index(entity);

        return entity;
    }

    // Get
    Entity_Shared EntityManager::get(const ID entity_id) const
    {
        auto ent_iter = entities_by_id.find(entity_id);
        if (ent_iter != entities_by_id.end())
        {
            Entity_Weak     ent_weak    = ent_iter->second;
            Entity_Shared   entity      = ent_weak.lock();

            if (entity)
            {
                return entity;
            }
        }

        // No match found.
        return nullptr;
    }

    std::vector<Entity_Shared> EntityManager::get() const
    {
        std::vector<Entity_Shared> all;
        all.assign(entities.begin(), entities.end());

        return all;
    }

    // Delete
    void EntityManager::remove(const ID entity_id)
    {
        Entity_Shared entity = get(entity_id);
        remove(entity);
    }

    void EntityManager::remove(Entity_Shared const& entity)
    {
        if (!entity)
        {
            std::cerr
                << "Could not delete entity: "
                << "Entity doesn't exist."
                << std::endl;

            return;
        }

        deindex(entity);
        entities.erase(entity);

        // Todo: Consolidate aspect change notifications.
        auto world = world_.lock();
        world->component_manager.remove(entity);
    }

    size_t EntityManager::size() const
    {
        return entities.size();
    }

    /*
     *  Protected
     */
    void EntityManager::index(Entity_Shared const& entity)
    {
        // Add to index maps.
        entities_by_id[entity->id] = entity;
    }

    void EntityManager::deindex(Entity_Shared const& entity)
    {
        // Remove from index maps.
        entities_by_id.erase(entity->id);
    }
}
