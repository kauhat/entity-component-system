#include "ecs/component_type_manager.h"
#include <iostream>

namespace ECS
{
    ComponentType_Shared ComponentTypeManager::registerType(ComponentType const& type)
    {
        if (types_by_name.find(type.name) != types_by_name.end())
        {
            std::cerr
                << "Could not register component type \""
                << type.name << "\": "
                << "Name in use."
                << std::endl;

            return getType(type.name);
        }

        auto type_pair = types.emplace(std::make_shared<ComponentType>(type));
        auto type_iter = type_pair.first;

        ComponentType_Shared type_ptr = *type_iter;
        types_by_name.insert(std::make_pair(type.name, type_ptr));

        return type_ptr;
    }

	//ComponentType_Shared ComponentTypeManager::getType(std::string name)
    ComponentType_Shared ComponentTypeManager::getType(std::string const& name)
    {
		if (this->types_by_name.empty())
		{
			return nullptr;
		}

        auto it = types_by_name.find(name);
        if (it == types_by_name.end())
        {
            std::cerr
                << "Could not get component type by name \""
                << name << "\": "
                << "Type not registered."
                << std::endl;

            return nullptr;
        }
        else
        {
            auto type_weak = types_by_name.at(name);
            return type_weak.lock();
        }
    }

    void ComponentTypeManager::unregisterType(std::string const& name)
    {
        auto it = types_by_name.find(name);
        if (it != types_by_name.end())
        {
            auto type = it->second.lock();
            unregisterType(type);
        }
    }

    void ComponentTypeManager::unregisterType(ComponentType_Shared type)
    {
        if (type == nullptr)
        {
            // Type does not exist.
            return;
        }

        auto it = types.find(type);
        if (it != types.end())
        {
            types.erase(type);
            types_by_name.erase(type->name);
        }
    }
}
