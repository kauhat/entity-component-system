#pragma once
#include "system.h"
#include "aspect.h"

#include <memory>

namespace ECS
{
	class EntitySystem : public System
	{
	public:
		~EntitySystem(){}
			
		std::shared_ptr<Aspect> getAspect();
		std::set<Entity_Shared> getEntities();
	
	protected:
		std::shared_ptr<Aspect> aspect_;

		// Virtuals
		virtual std::shared_ptr<Aspect> initializeAspect() = 0;
	};
}