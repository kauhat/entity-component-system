#pragma once
#include "ecs/common.h"
#include "ecs/system.h"
#include "ecs/manager.h"

#include <core/serialization.h>
#include <core/data/id_provider.hpp>
#include <typeinfo>
#include <typeindex>
#include <memory>
#include <map>

namespace ECS
{
    class SystemManager : public Manager
    {
public:
        void add(System_Shared const&);
        void process();

        size_t size() const;

protected:
        std::set<System_Shared>
        systems;

        std::unordered_map<std::type_index, System_Weak>
        systems_by_type;

        // Serialization.
        friend class cereal::access;

        template<class Archive>
        void serialize(Archive& ar)
        {
            ar(
                cereal::make_nvp("manager", cereal::base_class<Manager>(this))
                //,       cereal::make_nvp("systems", systems_)
                );
        }

private:
    };
}
