#pragma once
#include "ecs/world.h"

#include "ecs/entity.h"
#include "ecs/entity_manager.h"

#include "ecs/component.h"
#include "ecs/component_type.h"
#include "ecs/component_manager.h"

#include "ecs/system.h"
#include "ecs/system_manager.h"

#include "ecs/aspect.h"