#pragma once
#include "ecs/common.h"
#include "ecs/manager.h"
#include "ecs/aspect.h"

#include <core/serialization.h>
#include <memory>
#include <map>

namespace ECS
{
    class AspectSubject : public Manager
    {
public:
        void registerObserver(Aspect_Shared);
        void unregisterObserver(Aspect_Shared);

		void changed(Entity_Shared);
protected:
		std::vector<Aspect_Shared > subscribers;
    };
}