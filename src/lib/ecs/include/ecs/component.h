#pragma once
#include "ecs/common.h"
#include "ecs/entity.h"

#include <core/common.h>
#include <memory>

namespace ECS
{
    /*
     * Abstract class for use when deriving components.
     */
    class Component
    {
    public:
        // Types
        typedef Component_Shared (*CreateCallback)();

        //
        virtual ~Component() = 0;

        //
        Core::ID id;
        World_Weak world;
        Entity_Weak entity;
        ComponentType_Weak type;

		//
		bool isType(ComponentType_Shared const&);
		ComponentType_Shared getType();
    };

    class ComponentBuilder
    {
public:
        ComponentBuilder& id(Core::ID id);
        ComponentBuilder& world(World_Shared const& world);
        ComponentBuilder& entity(Entity_Shared const& entity);
        ComponentBuilder& type(ComponentType_Shared const& type);
        Component_Shared build() const;

protected:
        Core::ID id_;
        World_Shared world_;
        Entity_Shared entity_;
        ComponentType_Shared type_;
    };
}

/*
 * Cereal
 */
namespace cereal
{
    template<class Archive>
    void serialize(Archive& ar, ECS::Component& com)
    {
        ar(
            cereal::make_nvp("id", com.id),
            cereal::make_nvp("entity", com.entity)
            );
    }
}

CEREAL_REGISTER_TYPE(ECS::Component);
