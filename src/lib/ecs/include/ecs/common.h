#pragma once
#include <memory>

namespace ECS
{
    class World;
    typedef std::shared_ptr<World> World_Shared;
    typedef std::weak_ptr<World> World_Weak;

    class Aspect;
    typedef std::shared_ptr<Aspect> Aspect_Shared;
    typedef std::weak_ptr<Aspect> Aspect_Weak;

    class Entity;
    typedef std::shared_ptr<Entity> Entity_Shared;
    typedef std::weak_ptr<Entity> Entity_Weak;

    class Component;
    typedef std::shared_ptr<Component> Component_Shared;
    typedef std::weak_ptr<Component> Component_Weak;

    class ComponentType;
    typedef std::shared_ptr<ComponentType> ComponentType_Shared;
    typedef std::weak_ptr<ComponentType> ComponentType_Weak;

    class System;
    typedef std::shared_ptr<System> System_Shared;
    typedef std::weak_ptr<System> System_Weak;
}
