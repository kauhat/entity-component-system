#pragma once
#include "component.h"
#include "component_type.h"
#include "manager.h"

#include <unordered_map>
#include <string>
#include <memory>
#include <set>

namespace ECS
{
    class ComponentTypeManager : public Manager
    {
public:
        ComponentType_Shared registerType(ComponentType const&);
        ComponentType_Shared getType(std::string const&);
		
        void unregisterType(std::string const&);
        void unregisterType(ComponentType_Shared);

protected:
        std::set<ComponentType_Shared> types;
        std::unordered_map<std::string, ComponentType_Weak> types_by_name;

        // Serialization.
        friend class cereal::access;

        template<class Archive>
        void serialize(Archive& ar)
        {
            ar(
                cereal::make_nvp("manager", cereal::base_class<Manager>(this))
                ,       cereal::make_nvp("types", types)
                );
        }
    };
}
