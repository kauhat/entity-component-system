#pragma once
#include "ecs/common.h"

#include <core/common.h>
#include <core/serialization.h>
#include <memory>
#include <map>

namespace ECS
{
    class Entity
    {
    public:
        Core::ID id;
        World_Weak world;

        // Serialization.
        friend class cereal::access;

        template<class Archive>
        void serialize(Archive& ar)
        {
            ar(
                cereal::make_nvp("id", id)
                );
        }
    };

    class EntityBuilder
    {
public:
        EntityBuilder& id(Core::ID id);
        EntityBuilder& world(World_Shared world);
        Entity_Shared build() const;

protected:
        Core::ID id_;
        World_Shared world_;
    };
}
