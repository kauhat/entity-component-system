#pragma once
#include "ecs/common.h"

#include <core/serialization.h>
#include <memory>
#include <set>

namespace ECS
{
    struct AspectInterest
    {
        std::set<ComponentType_Shared> all;     // all of the components in this set.
        std::set<ComponentType_Shared> any;     // any of the components in this set.
        std::set<ComponentType_Shared> none;    // none of the components in this set.
    };

    class Aspect : public std::enable_shared_from_this<Aspect>
    {
    public:
        void setWorld(World_Shared const&);
        World_Shared getWorld() const;

        void setInterest(AspectInterest const&);
        AspectInterest getInterest() const;

        //
        bool interested(std::set<ComponentType_Shared>);
        std::set<Entity_Shared> getEntities() const;

        //
        void notify(Entity_Shared const&);
        
    protected:
        World_Weak world_;

        // Aspect interested in entities containing...
        AspectInterest interest_;
        
        // Matching entities cache.
        std::set<Entity_Shared> matched_;

        void register_world();
        void unregister_world();
    };


    /*class AspectBuilder
    {
public:
        AspectBuilder& world(World_Shared world);
        AspectBuilder& all(std::set<ComponentType_Shared>);
        AspectBuilder& any(std::set<ComponentType_Shared>);
        AspectBuilder& none(std::set<ComponentType_Shared>);
        Aspect_Shared build() const;

protected:
        Core::ID id_;
        std::set<ComponentType_Shared> all_, any_, none_;
    };*/
}