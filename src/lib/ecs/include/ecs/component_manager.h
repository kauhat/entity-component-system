#pragma once
#include "ecs/manager.h"
#include "ecs/component.h"
#include "ecs/component_type_manager.h"

#include <core/common.h>
#include <core/data/id_provider.hpp>

#include <memory>
#include <map>

namespace ECS
{
    class ComponentManager : public Manager
    {
public:
        // Create a component.
        Component_Shared create(Entity_Shared const&, ComponentType_Shared const&);

        // Get single component.
        Component_Shared get(const Core::ID);
        Component_Shared get(Entity_Shared const&, ComponentType_Shared const&);

        
        // Get multiple components.
        std::set<Component_Shared> get(Entity_Shared const&);
        std::set<Component_Shared> get(ComponentType_Shared const&);

        //std::vector<Component_Shared> get(Entity_Shared const&);
        //std::vector<Component_Shared> get(ComponentType_Shared const&);


        // Remove a component.
        void remove(const Core::ID);
        void remove(Component_Shared const&);

        // Remove multiple components
        void remove(Entity_Shared const&);

        //
        size_t size() const;

        // Utility functions.
        static std::set<ComponentType_Shared> getTypes(std::set<Component_Shared>);

        //
        ComponentTypeManager types;

protected:
        typedef std::set<Component_Weak, std::owner_less<Component_Weak> > Set_Weak;

        //
        void index(Component_Shared const&);
        void deindex(Component_Shared const&);

        //
        Core::IdProvider<Core::ID> id;
        
        // Component set
        std::set<Component_Shared>
        components;

        // Component indexes
        std::map<Core::ID, Component_Weak >
        components_by_id;

        std::map<ComponentType_Weak, Set_Weak, std::owner_less<ComponentType_Weak> >
        components_by_type;

        std::map<Entity_Weak, Set_Weak, std::owner_less<Entity_Weak> >
        components_by_entity;

        // Serialization.
        friend class cereal::access;

        template<class Archive>
        void save(Archive& archive) const
        {
            archive(
                cereal::make_nvp("manager", cereal::base_class<Manager>(this))
                ,       cereal::make_nvp("components", components)
                ,       cereal::make_nvp("type manager", types)
                );
        }

        template<class Archive>
        void load(Archive& archive)
        {
            archive(
                cereal::make_nvp("manager", cereal::base_class<Manager>(this))
                ,       cereal::make_nvp("components", components)
                ,       cereal::make_nvp("type manager", types)
                );

            for (auto& component : components)
            {
                index(component);
            }
        }
    };
}

CEREAL_SPECIALIZE_FOR_ALL_ARCHIVES(ECS::ComponentManager, cereal::specialization::member_load_save)
