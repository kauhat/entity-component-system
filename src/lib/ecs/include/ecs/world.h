#pragma once
#include "ecs/common.h"
#include "ecs/entity_manager.h"
#include "ecs/component_manager.h"
#include "ecs/system_manager.h"
#include "ecs/aspect_subject.h"

#include <chrono>

namespace ECS
{
    class World
    {
public:
        EntityManager       entity_manager;
        ComponentManager    component_manager;
        SystemManager       system_manager;
        AspectSubject       aspect_subject;

        //void setDelta(std::chrono::system_clock::duration duration);
        //void process();

protected:
        // Serialization.
        friend class cereal::access;

        template<class Archive>
        void serialize(Archive& ar)
        {
            ar(
                cereal::make_nvp("entity manager", entity_manager)
                ,       cereal::make_nvp("component manager", component_manager)
                ,       cereal::make_nvp("system manager", system_manager)
                );
        }
    };

    class WorldBuilder
    {
public:
        WorldBuilder();
        World_Shared build();

protected:
        World_Shared world_;
    };
}