#pragma once
#include "component.h"

#include <memory>
#include <string>
#include <unordered_map>

namespace ECS
{
    class ComponentType
    {
    public:
        std::string name;
        Component::CreateCallback factory;
    };

    class ComponentTypeBuilder
    {
public:
        ComponentTypeBuilder& name(const std::string& name);
        ComponentTypeBuilder& factory(Component::CreateCallback factory);

        ComponentType build() const;

protected:
        std::string name_;
        Component::CreateCallback factory_;
    };
}

/*
 * Cereal
 */
namespace cereal
{
    template<class Archive>
    void serialize(Archive& ar, ECS::ComponentType& type)
    {
        ar(
            cereal::make_nvp("name", type.name)
            );
    }
}
