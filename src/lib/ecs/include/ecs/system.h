#pragma once
#include "ecs/common.h"

#include <core/serialization.h>
#include <memory>

namespace ECS
{
    class System
    {
public:
        // Virtual
        virtual ~System() = 0;
        virtual void processSystem() = 0;

        //
        void process();

        //
        World_Shared getWorld();
        void setWorld(World_Shared);

protected:
        World_Weak world_;
        bool enabled_ = true;
    };
}