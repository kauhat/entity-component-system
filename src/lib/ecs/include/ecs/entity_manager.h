#pragma once
#include "ecs/common.h"
#include "ecs/entity.h"
#include "ecs/manager.h"

#include <core/common.h>
#include <core/serialization.h>
#include <core/data/id_provider.hpp>

#include <memory>
#include <map>

namespace ECS
{
    class EntityManager : public Manager
    {
public:
        // Create an entity.
        Entity_Shared create();

        // Get single entity.
        Entity_Shared get(const Core::ID) const;

        // Get multiple entities.
        std::vector<Entity_Shared> get() const;

        // Remove an entity.
        void remove(const Core::ID);
        void remove(Entity_Shared const&);

        //
        size_t size() const;

        // Utility functions
        bool registered(Entity_Shared const&);

protected:
        void index(Entity_Shared const&);
        void deindex(Entity_Shared const&);

        std::set<Entity_Shared>     entities;
        std::map<Core::ID, Entity_Weak>   entities_by_id;

        Core::IdProvider<Core::ID> id;

        // Serialization.
        friend class cereal::access;

        template<class Archive>
        void save(Archive& ar) const
        {
            ar(
                cereal::make_nvp("manager", cereal::base_class<Manager>(this))
                ,       cereal::make_nvp("entities", entities)
                );
        }

        template<class Archive>
        void load(Archive& ar)
        {
            ar(
                cereal::make_nvp("manager", cereal::base_class<Manager>(this))
                ,       cereal::make_nvp("entities", entities)
                );

            for (auto& entity : entities)
            {
                index(entity);
            }
        }
    };
}

CEREAL_SPECIALIZE_FOR_ALL_ARCHIVES(ECS::EntityManager, cereal::specialization::member_load_save)