#pragma once
#include "ecs/common.h"

#include <core/common.h>
#include <core/serialization.h>

namespace ECS
{
    class Manager
    {
public:
        World_Shared getWorld() const;
        void setWorld(World_Shared const& world);

protected:
        World_Weak world_;

        // Serialization.
        friend class cereal::access;

        template<class Archive>
        void serialize(Archive& ar)
        {
            ar(
                cereal::make_nvp("world", world_)
                );
        }
    };
}
