set(INCLUDE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/include)

add_library(Ascii INTERFACE)

target_include_directories(Ascii INTERFACE
	${INCLUDE_DIR})