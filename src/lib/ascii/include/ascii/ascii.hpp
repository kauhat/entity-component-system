#pragma once

// Some useful character codes.
namespace Ascii
{
	// Box characters.
	const unsigned char BOX_H 	= 0xC4;
    const unsigned char BOX_V 	= 0xB3;
    const unsigned char BOX_DL	= 0xBF;
    const unsigned char BOX_DR	= 0xDA;
    const unsigned char BOX_UL	= 0xD9;
    const unsigned char BOX_UR	= 0xC0;
    const unsigned char BOX_VL	= 0xB4;
    const unsigned char BOX_VR	= 0xC3;
    const unsigned char BOX_VH	= 0xC5;
}