#include "console/command.h"
#include "console/console.h"

#include <ascii/ascii.hpp>
#include <iostream>
#include <stack>
#include <set>

using namespace Ascii;

namespace CLI
{
    /*
     * Command
     */
    void Command::init()
    {
        std::shared_ptr<Command> self = this->shared_from_this();
        node = std::make_shared<TreeNode>(TreeNode(self));
    }

    void Command::run(Context ctx)
    {
        if (fp)
        {
            fp(ctx);
        }
        else
        {
            print();
        }
    }

    struct StackNode
    {
        std::shared_ptr<Command::TreeNode> node;
        int depth;
    };

    void Command::print()
    {
        std::cout << "\n";

        std::stack<StackNode> stack;
        stack.push({this->node, 0});

        while (!stack.empty())
        {
            //Take the top item off of the stack.
            StackNode ref = stack.top();
            stack.pop();

            if (ref.node)
            {
                // Add child nodes to stack.
                if (!ref.node->isLeaf())
                {
                    auto& children = ref.node->children;
                    for (auto r_it = children.rbegin(); r_it != children.rend(); r_it++)
                    {
                        StackNode next;
                        next.node   = (*r_it);
                        next.depth  = ref.depth + 1;

                        stack.push(next);
                    }
                }

                // Print node information.
                auto owner = ref.node->owner.lock();
                if (owner)
                {
                    std::cout
                        << std::string(Console::INDENT_SIZE * ref.depth, ' ')
                        << BOX_VR << " " << owner->name
                        << "\n";
                }

                // Print tree lines.
                if(!stack.empty())
                {
                    auto next = stack.top();

                    if(next.depth > ref.depth)
                    {
                        // Node is first child.
                        size_t indent   = (Console::INDENT_SIZE * ref.depth);

                        std::cout
                            << std::string(indent, ' ')
                            << BOX_UR << std::string(Console::INDENT_SIZE - 1, ' ') << BOX_DL
                            << "\n";
                    }
                    else if(next.depth < ref.depth)
                    {
                        // Nodes is last child.
                        size_t indent   = (Console::INDENT_SIZE * next.depth);
                        size_t width    = (Console::INDENT_SIZE * (ref.depth - next.depth)) - 2;

                        std::cout
                            << std::string(indent, ' ')
                            << BOX_DR << std::string(width+1, ' ') << BOX_UL
                            << "\n";
                    }
                }
            }
        }

        std::cout << "\n" << std::flush;
    }

    std::shared_ptr<Command> Command::findChild(std::string name)
    {
        std::shared_ptr<Command> child;

        for (auto it = node->children.begin(); it != node->children.end(); it++)
        {
            auto child_node = std::static_pointer_cast<Command::TreeNode>(*it);
            if(child_node)
            {
                auto owner = child_node->owner.lock();
                if (owner && owner->name == name)
                {
                    child = owner;
                    break;
                }
            }
        }

        return child;
    }

    Command::Search Command::findChild(std::vector<std::string> path)
    {
        Search search_result = Search();

        // Add root node to command to stack.
        std::stack<std::shared_ptr<Command>> command_stack;
        command_stack.push(this->shared_from_this());

        //
        unsigned int depth = 0;
        while (!command_stack.empty())
        {
            std::shared_ptr<Command> command = command_stack.top();
            command_stack.pop();

            if (command)
            {
                search_result.depth     = depth;
                search_result.deepest   = command;

                if (depth < path.size())
                {
                    // Node is not deepest possible match.
                    auto next = command->findChild(path[depth]);
                    command_stack.push(next);
                }
                else
                {
                    // Node found is deepest possible match.
                    search_result.found = true;
                }

                depth++;
            }
        }

        return search_result;
    }

    /*
     * Builder
     */
    CommandBuilder& CommandBuilder::name(std::string name)
    {
        name_ = name;

        return *this;
    }

    CommandBuilder& CommandBuilder::sypnosis(std::string sypnosis)
    {
        sypnosis_ = sypnosis;

        return *this;
    }

    CommandBuilder& CommandBuilder::description(std::string description)
    {
        description_ = description;

        return *this;
    }

    CommandBuilder& CommandBuilder::functionPointer(Command::FunctionPointer function)
    {
        fp_ = function;

        return *this;
    }

    std::shared_ptr<Command> CommandBuilder::build() const
    {
        std::shared_ptr<Command> command = std::make_shared<Command>();
		command->fp = fp_;
        command->name        = name_;
        command->sypnosis    = sypnosis_;
        command->description = description_;
        command->init();
        
        return command;
    }
}
