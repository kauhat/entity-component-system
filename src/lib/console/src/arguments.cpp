#include "console/arguments.h"
#include "console/console.h"

namespace CLI
{
    const std::string Arguments::OPTION_PREFIX = "--";

    Arguments::Arguments(std::vector<std::string> const& words)
    {
        for (auto wordsIt = words.begin(); wordsIt != words.end(); ++wordsIt)
        {
            std::string const& word = *wordsIt;

            if (isOption(word))
            {
                // is option
                std::string option_name, option_value;
                option_name = word.substr(Arguments::OPTION_PREFIX.size(), word.size());

                // get option value
                auto nextIt = std::next(wordsIt);
                if (nextIt != words.end() && !isOption(*nextIt))
                {
                    option_value = *nextIt;

                    std::advance(wordsIt, 1);
                }

                //
                auto opsIt = options.find(option_name);
                if (opsIt != options.end())
                {
                    // map key already exists
                    opsIt->second = option_value;
                }
                else
                {
                    // map key doesn't exist
                    options.insert(make_pair(option_name, option_value));
                }
            }
            else
            {
                // is argument
                arguments.push_back(word);
            }
        }
    }

    bool Arguments::isOption(std::string const& word)
    {
        return Arguments::OPTION_PREFIX.length() < word.length()
               && Arguments::OPTION_PREFIX
                  == word.substr(0, Arguments::OPTION_PREFIX.size());

    }

    void Arguments::print()
    {
        std::cout << "Options:" << "\n";

        for (auto option_pair : options)
        {
            std::cout   << std::string(Console::INDENT_SIZE, ' ')
                        << option_pair.first << ": " << option_pair.second << "\n";
        }

        std::cout   << "\n"
                    << "Arguments:" << "\n";

        for (auto argument : arguments)
        {
            std::cout   << std::string(Console::INDENT_SIZE, ' ')
                        << argument << "\n";
        }

        std::cout << "\n" << std::flush;
    }
}