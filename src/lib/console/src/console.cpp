#include "console/console.h"

#include <generic_system/filesystem.h>
#include <utility/utility.hpp>
#include <iostream>
#include <memory>

namespace CLI
{
    /*
     * Console
     */
    Console::Console()
    {
        root = std::make_shared<Command>();
		root->name = "root";
        root->init();
    }

    void Console::init()
    {
        std::cout << "ECS Interactive Console" << "\n";
        std::cout << "(" << System::getExecutablePath() << ")" << "\n";
        std::cout << "\n" << std::flush;

        wait();
    }

    std::string Console::getInput()
    {
        std::cout << ">" << std::flush;

        std::string line;
        std::getline(std::cin, line);

        return line;
    }

    void Console::wait()
    {
        while (running)
        {
            // Wait for a line in the terminal.
            std::string line = getInput();

            if (line.length() > 0)
            {
                std::cout << "\n";

                std::vector<std::string> words;
                Utility::split(line, ' ', words);

                Arguments args(words);

                // Get a command.
                auto search = root->findChild(args.arguments);

                if (search.found || search.deepest)
                {
                    // Remove command path arguments.
                    args.arguments.erase(args.arguments.begin(), args.arguments.begin() + search.depth);

                    // Construct context for use with command function.
                    Context context = {*this, args};

                    // Command has function.
                    if (search.deepest->fp != nullptr)
                    {
                        // Legit command found.
                        search.deepest->fp(context);
                    }
                    else
                    {
                        // Branch command found.
                        if(search.deepest->node->isRoot())
                        {
                            std::cout
                                << "Available commands:"
                                << "\n";
                        }
                        else
                        {
                            std::cout
                                << "Available commands for "
                                << "\"" << search.deepest->name << "\":"
                                << "\n";
                        }

                        search.deepest->print();
                    }
                }
                else
                {
                    // No command found.
                    std::cout << "Command \"" << args.arguments[0] << "\" not found." << "\n";
                }
            }
        }
    }

    void Console::registerCommands(std::vector<std::shared_ptr<Command>> new_commands)
    {
        for(auto command : new_commands)
        {
            if(command)
            {
                // If the command is top level, append it to the root node.
                if(command->node->isRoot())
                {
                    root->node->appendChild(command->node);
                }

                // Add command to internal vector.
                commands.push_back(command);
            }
        }
    }
}