#pragma once
#include <iostream>
#include <memory>
#include <string>
#include <vector>
#include <map>

namespace CLI
{
    class Arguments
    {
public:
        static const std::string OPTION_PREFIX;

        explicit Arguments(std::vector<std::string> const& words);

        void print();
        bool isOption(std::string const& word);

        std::map<std::string, std::string>  options;
        std::vector<std::string>            arguments;
    };
}