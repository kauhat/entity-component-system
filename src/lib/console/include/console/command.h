#pragma once
#include "arguments.h"
#include <tree/composable_node.hpp>

#include <string>
#include <vector>
#include <memory>

namespace CLI
{
    class Console;

    struct Context
    {
        Console& console;
        Arguments args;
    };

    class Command : public std::enable_shared_from_this<Command>
    {
    public:
        struct Search
        {
            bool found = false;
            unsigned int depth = 0;
            std::shared_ptr<Command> deepest;
        };

        //
        typedef void (*FunctionPointer)(Context);
        typedef Tree::ComposableNode<Command> TreeNode;

        //
        void init();
        void print();
        void run(Context);

        std::shared_ptr<Command> findChild(std::string name);
        Search findChild(std::vector<std::string> path);

        //
        std::shared_ptr<TreeNode> node;

        std::string name;
        std::string sypnosis;
        std::string description;
        FunctionPointer fp = nullptr;

    protected:

    };

    class CommandBuilder
    {
    public:
        std::shared_ptr<Command> build() const;

        CommandBuilder& name(std::string);
        CommandBuilder& sypnosis(std::string);
        CommandBuilder& description(std::string);
        CommandBuilder& functionPointer(Command::FunctionPointer);

    protected:
        std::string name_;
        std::string sypnosis_;
        std::string description_;
        Command::FunctionPointer fp_;
    };
}
