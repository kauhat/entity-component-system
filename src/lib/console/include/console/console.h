#pragma once
#include <iostream>
#include <memory>
#include <string>
#include <vector>
#include <map>

#include "command.h"
#include "arguments.h"

namespace CLI
{
    class Console
    {
    public:
        static const size_t INDENT_SIZE = 4;

        Console();

        void init();
        void registerCommands(std::vector<std::shared_ptr<Command>>);
        
        std::shared_ptr<Command> root;
        std::vector<std::shared_ptr<Command>> commands;
        
    protected:
        struct CommandSearch
        {
            bool found  = false;
            int depth   = 0;
            std::shared_ptr<Command> command;
        };
        
        bool running = true;

        void wait();
        std::string getInput();
        
    };
}
