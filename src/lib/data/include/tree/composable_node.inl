namespace Tree
{
    template<typename T>
    ComposableNode<T>::ComposableNode()
    {}

    template<typename T>
    ComposableNode<T>::ComposableNode(std::shared_ptr<T> node_owner) : owner(node_owner)
    {}
}