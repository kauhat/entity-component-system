#include <iostream>

// Generic node template.
namespace Tree
{
    template<typename Derived>
    NodeBase<Derived>::~NodeBase()
    {
        unregisterParent();
        unregisterChildren();
    }

    template<typename Derived>
    bool NodeBase<Derived>::isLeaf()
    {
        return children.empty();
    }

    template<typename Derived>
    bool NodeBase<Derived>::isRoot()
    {
        return (parent == nullptr);
    }

    // Getter functions.
    template<typename Derived>
    std::shared_ptr<Derived> NodeBase<Derived>::getParent()
    {
        return parent;
    }

    template<typename Derived>
    std::set<std::shared_ptr<Derived>> const& NodeBase<Derived>::getChildren()
    {
        return children;
    }

    template<typename Derived>
    std::set<std::shared_ptr<Derived>> const& NodeBase<Derived>::getSiblings()
    {
        if (parent != nullptr)
        {
            return parent->getChildren();
        }
        else
        {
            return std::set<std::shared_ptr<Derived>>();
        }
    }

    // Setter functions.
    template<typename Derived>
    void NodeBase<Derived>::setParent(const std::shared_ptr<Derived>& parent)
    {
        unregisterParent();

        if(parent)
        {
            this->parent = parent;
            parent->appendChild(&this);
        }
    }

    template<typename Derived>
    void NodeBase<Derived>::appendChild(const std::shared_ptr<Derived>& child)
    {
        child->parent = std::static_pointer_cast<Derived>(this->shared_from_this());
        children.insert(child);
    }

    template<typename Derived>
    void NodeBase<Derived>::removeChild(const std::shared_ptr<Derived>& child)
    {
        child->parent = nullptr;
        children.erase(child);
    }

    // Protected functions.
    template<typename Derived>
    void NodeBase<Derived>::unregisterParent()
    {
        if (parent)
        {
            parent->removeChild(std::static_pointer_cast<Derived>(this->shared_from_this()));
        }
    }

    template<typename Derived>
    void NodeBase<Derived>::unregisterChildren()
    {
        for(auto child : children)
        {
            child->parent = nullptr;
        }
    }
}
