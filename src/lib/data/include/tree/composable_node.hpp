#pragma once
#include "node.hpp"

#include <memory>

// Composable node template.
namespace Tree
{
    template<typename T>
    class ComposableNode : public NodeBase<ComposableNode<T> >
    {
public:
        ComposableNode();
        ComposableNode(std::shared_ptr<T>);

        //
        std::weak_ptr<T> owner;
    };
}

#include "composable_node.inl"
