#pragma once
#include <set>
#include <memory>

namespace Tree
{
    // Base node template.
    template<typename Derived>
    class NodeBase : public std::enable_shared_from_this<NodeBase<Derived> >
    {
public:
        NodeBase(){}
        ~NodeBase();

        bool isLeaf();
        bool isRoot();

        std::shared_ptr<Derived> getParent();
        std::set<std::shared_ptr<Derived>> const& getChildren();
        std::set<std::shared_ptr<Derived>> const& getSiblings();

        void setParent(const std::shared_ptr<Derived>&);
        void appendChild(const std::shared_ptr<Derived>&);
        void removeChild(const std::shared_ptr<Derived>&);

        //
        std::shared_ptr<Derived> parent = nullptr;
        std::set<std::shared_ptr<Derived>> children;

protected:
        void unregisterParent();
        void unregisterChildren();
        
    };
}

#include "node.inl"
