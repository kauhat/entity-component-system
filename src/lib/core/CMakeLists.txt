set(INCLUDE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/include)
set(SOURCE_DIR  ${CMAKE_CURRENT_SOURCE_DIR}/src)

set(SOURCE_FILES
	${SOURCE_DIR}/service.cpp
	${SOURCE_DIR}/program_settings.cpp)

add_library(EngineCore STATIC
	${SOURCE_FILES})

target_include_directories(EngineCore PUBLIC
	"${VENDOR_ROOT}/cereal/include"
	${INCLUDE_DIR})
