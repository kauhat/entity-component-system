#pragma once
#include <unordered_map>
#include <typeinfo>
#include <typeindex>
#include <memory>
#include <iostream>

namespace Core
{
    class Locator
    {
public:
        /*
         * Register a service provider.
         */
        template<typename T>
        static void provide(std::shared_ptr<T> service)
        {
            std::type_index key     = typeid(T);
            ServiceMap::iterator it = services.find(key);

            if (it != services.end())
            {
                std::cerr
                << "Service Locator: "
                << "Object of type " << key.name() << " already registered."
                << std::endl;
            }
            else
            {
                services.insert(std::make_pair(key, service));
            }
        }

        /*
         * Get a previously registered provider.
         */
        template<typename T>
        static std::weak_ptr<T> getService()
        {
            std::type_index key     = typeid(T);
            ServiceMap::iterator it = services.find(key);

            if (it != services.end())
            {
                std::weak_ptr<T> service = std::static_pointer_cast<T>(it->second);
                return service;
            }
            else
            {
                std::cerr
                << "Service Locator: "
                << "No object of type " << key.name() << " is registered."
                << std::endl;

                return std::weak_ptr<T>();
            }
        }

protected:
        typedef std::unordered_map<std::type_index, std::shared_ptr<void> > ServiceMap;

        static ServiceMap services;
    };
}