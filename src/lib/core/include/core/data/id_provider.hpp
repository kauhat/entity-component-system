#pragma once
#include <type_traits>

namespace Core
{
    template<typename T>
    class IdProvider
    {
public:
        T getNext()
    	{
    	    return next_id++;
    	}
protected:
        T next_id;
    };
}
