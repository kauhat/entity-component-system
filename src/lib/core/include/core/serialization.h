#pragma once

// Serialization helpers.
#include <cereal/access.hpp>

// Serialization types.
#include <cereal/types/vector.hpp>
#include <cereal/types/set.hpp>
#include <cereal/types/map.hpp>
#include <cereal/types/unordered_map.hpp>
#include <cereal/types/memory.hpp>
#include <cereal/types/polymorphic.hpp>

// Serialization archives.
#include <cereal/archives/json.hpp>
#include <cereal/archives/binary.hpp>
