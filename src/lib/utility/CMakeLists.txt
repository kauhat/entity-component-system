set(INCLUDE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/include)

add_library(Utility INTERFACE)

target_include_directories(Utility INTERFACE
	${INCLUDE_DIR})