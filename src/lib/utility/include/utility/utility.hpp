#ifndef UTILITY_HPP
#define UTILITY_HPP

#include <vector>
#include <map>
#include <string>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <unordered_map>

namespace Utility
{
    /*
     * String utility functions.
     */

    //
    inline std::vector<std::string>& split(std::string const& s, char delim, std::vector<std::string>& elems)
    {
        std::stringstream ss(s);
        std::string item;

        while (std::getline(ss, item, delim))
        {
            if (!item.empty())
            {
                elems.push_back(std::move(item));
            }
        }

        return elems;
    }

    //
    inline std::vector<std::string> split(std::string const& s, char delim)
    {
        std::vector<std::string> elems;
        split(s, delim, elems);

        return elems;
    }

    /*
     * Stream Utilities
     */

    struct FormatCache
    {
        char fill;
        std::ios_base::fmtflags flags;
        std::streamsize precision;
    };

    static std::unordered_map<std::ostream*, FormatCache> cache_map;

    // Set Hex flags
    inline std::ostream& sethex(std::ostream& stream)
    {
        const char  FILL    = '0';
        const int   WIDTH   = 8;

        using namespace std;

        FormatCache& cache = cache_map[&stream];
        cache.fill = stream.fill();
        cache.flags     = stream.flags();
        cache.precision = stream.precision();

        stream.fill(FILL);
        stream.unsetf(ios::dec | ios::oct);
        stream.setf(ios::hex | ios::showbase | ios::internal);
        stream.width(WIDTH);

        return stream;
    }

    // Reset flags.
    inline std::ostream& reset(std::ostream& stream)
    {
        using namespace std;

        if (cache_map.find(&stream) != cache_map.end())
        {
            FormatCache& cache = cache_map.at(&stream);
            stream.fill(cache.fill);
            stream.flags(cache.flags);
            stream.width(cache.precision);

            cache_map.erase(&stream);
        }

        return stream;
    }
}

#endif /* UTILITY_HPP */