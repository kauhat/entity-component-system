#pragma once

#if defined(_WIN32)
    #include "detail/library_windows.h"
#elif defined(__linux__)
    #include "detail/library_posix.h"
#endif

//Define system dependent library classes.
namespace System
{
    #if defined(_WIN32)
    typedef Detail::LibraryWindows  Library;
    #elif defined(__linux__)
    typedef Detail::LibraryPOSIX    Library;
    #endif
}