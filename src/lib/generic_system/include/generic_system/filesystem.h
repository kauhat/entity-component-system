#pragma once

#if defined(_WIN32)
    #include "detail/filesystem_windows.h"
#elif defined(__linux__)
    #include "detail/filesystem_posix.h"
#endif