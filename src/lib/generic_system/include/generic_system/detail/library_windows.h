#pragma once
#if defined(_WIN32)
    #include "library_base.h"
    #include <windows.h>

namespace System
{
    namespace Detail
    {
        class DLL_PUBLIC LibraryWindows : public LibraryBase
        {
public: ~LibraryWindows();

            bool load(std::string const& path) override;
            bool unload() override;
            void*   getSymbol(std::string const& name) override;

private:
            HMODULE handle;
        };
    }
}

#endif