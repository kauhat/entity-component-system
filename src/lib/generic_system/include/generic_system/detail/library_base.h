#pragma once
#include <string>
#include <exception>

#define DLL_PUBLIC
#define LIBRARY_EXPORT_MACROS

//Define our DLL_PUBLIC and DLL_LOCAL macros.
#ifndef LIBRARY_EXPORT_MACROS
    #if defined _WIN32 || defined __CYGWIN__
        #ifdef BUILDING_DYNAMIC_LIBRARY
            #ifdef __GNUC__
                #define DLL_PUBLIC __attribute__ ((dllexport))
            #else
                #define DLL_PUBLIC __declspec(dllexport)
            #endif
        #else
            #ifdef __GNUC__
                #define DLL_PUBLIC __attribute__ ((dllimport))
            #else
                #define DLL_PUBLIC __declspec(dllimport)
            #endif
        #endif
        #define DLL_LOCAL
    #else
        #if __GNUC__ >= 4
            #define DLL_PUBLIC __attribute__ ((visibility("default")))
            #define DLL_LOCAL  __attribute__ ((visibility("hidden")))
        #else
            #define DLL_PUBLIC
            #define DLL_LOCAL
        #endif
    #endif
#endif

namespace System
{
    namespace Detail
    {
        class DLL_PUBLIC LibraryBase
        {
public:
            virtual ~LibraryBase(){};

            virtual bool load(std::string const& path) = 0;
            virtual bool unload() = 0;
            virtual void*   getSymbol(std::string const& name) = 0;
        };
    }
}