#pragma once
#include <string>
#include <exception>

namespace System
{
    std::string getExecutablePath();
}