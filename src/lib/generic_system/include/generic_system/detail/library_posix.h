#pragma once
#if defined(__linux__)
    #include "library_base.h"
    #include <dlfcn.h>

namespace System
{
    namespace Detail
    {
        class DLL_PUBLIC LibraryPOSIX : public LibraryBase
        {
public: ~LibraryPOSIX();

            bool load(std::string const& path) override;
            bool unload() override;
            void*   getSymbol(std::string const& name) override;

private:
            void* handle;
        };
    }
}

#endif