#if defined(_WIN32)
#include "generic_system/detail/library_windows.h"
#include <iostream>

namespace System
{
    namespace Detail
    {
        LibraryWindows::~LibraryWindows(){}

        bool LibraryWindows::load(std::string const& path)
        {
            HMODULE library_handle = LoadLibrary(path.c_str());

            if (library_handle == NULL)
            {
                return false;
            }
            else
            {
                this->handle = library_handle;
                return true;
            }
        }

        bool LibraryWindows::unload()
        {
            if (FreeLibrary(this->handle))
            {
                //Successful
                return true;
            }
            else
            {
                //Failed
                return false;
            }
        }

        void* LibraryWindows::getSymbol(std::string const& name)
        {
            FARPROC symbol = GetProcAddress(this->handle, name.c_str());
            if (symbol == NULL)
            {
                throw "@todo: symbol not found.";
            }
            else
            {
                return (void*) symbol;
            }
        }
    }
}

#endif
