#if defined(_WIN32)
#include "generic_system/detail/filesystem_windows.h"
#include <windows.h>
#include <iostream>
#include <string>
#include <Shlwapi.h>

namespace System
{
    std::string getExecutablePath()
    {
        TCHAR   cstring[MAX_PATH];
        DWORD   status = GetModuleFileName(NULL, cstring, sizeof(cstring));

        if (status != 0)
        {
            std::string path(cstring);
            return path;
        }
        else
        {
            // Return empty string
            return std::string();
        }
    }
}

#endif