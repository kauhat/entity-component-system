#if defined(__linux__)
#include "generic_system/detail/library_posix.h"
#include <iostream>

namespace System
{
    namespace Detail
    {
        LibraryPOSIX::~LibraryPOSIX(){}

        bool LibraryPOSIX::load(std::string const& path)
        {
            std::cout << "Opening library \"" << path.c_str() << "\"." << std::endl;

            void* library_handle = dlopen(path.c_str(), RTLD_LAZY);

            if (library_handle == nullptr)
            {
                std::cout << "Unable to find shared library \"" << path << "\"" <<
                std::endl;
                return false;
            }
            else
            {
                this->handle = library_handle;
                return true;
            }
        }

        bool LibraryPOSIX::unload()
        {
            if (dlclose(this->handle) == 0)
            {
                //Successful
                return true;
            }
            else
            {
                //Failed
                return false;
            }
        }

        void* LibraryPOSIX::getSymbol(std::string const& name)
        {
            void* symbol = dlsym(this->handle, name.c_str());
            if (symbol == nullptr)
            {
                throw "@todo: symbol not found.";
            }
            else
            {
                return symbol;
            }
        }
    }
}

#endif