#include "catch.hpp"
#include "common.h"

#include "components/test.h"

#include <ecs/ecs.h>

using namespace ECS;

SCENARIO("Components can be created and removed", "[component manager]")
{
    // Setup world
    std::shared_ptr<World> world = WorldBuilder().build();

    // Grab useful objects.
    EntityManager&      entities    = world->entity_manager;
    ComponentManager&   components  = world->component_manager;

    // Register a component type.
    std::shared_ptr<ComponentType> type;
    type = components.types.registerType(Components::Test::getType());

    GIVEN("An empty entity world")
    {
        WHEN("Creating a component for an entity")
        {
            // Create entity.
            std::shared_ptr<Entity> entity;
            entity = entities.create();

            // Create component
            std::shared_ptr<Component> component;
            component = components.create(entity, type);

            THEN("The component exists")
            {
                REQUIRE(component != nullptr);
                REQUIRE(component->type.lock()      == type);
                REQUIRE(component->world.lock()     == world);
                REQUIRE(component->entity.lock()    == entity);
            }

            // Todo: move.
            THEN("The component can be cast to a useful type")
            {
                auto test = std::static_pointer_cast<Components::Test>(component);
                test->touched = true;
            }
        }

        WHEN("Creating multiple components")
        {
            size_t before;
            ptrdiff_t diff;

            before = components.size();

            // Batch create components.
            int amount = ::iterations;
            for(int i = 0; i < amount; i++)
            {
                components.create(
                    entities.create(),
                    type);
            }

            THEN("The amount of components has increased the amount created")
            {
				diff = (components.size() - before);
                REQUIRE(diff == amount);
            }
        }

        WHEN("Creating a component for an entity that does not exist")
        {
            // Create dud entity.
            std::shared_ptr<Entity> entity;
            entity = nullptr;

            // Create component
            std::shared_ptr<Component> component;
            component = components.create(entity, type);

            THEN("The component is null")
            {
                REQUIRE(component == nullptr);            
            }
        }

        WHEN("Creating a component for an entity from another world")
        {
            std::shared_ptr<World> another_world = WorldBuilder().build();
            
            // Create entity.
            std::shared_ptr<Entity> entity;
            entity = another_world->entity_manager.create();

            // Create component
            std::shared_ptr<Component> component;
            component = components.create(entity, type);

            THEN("The component is null")
            {
                REQUIRE(component == nullptr);            
            }
        }
    }

    GIVEN("A populated entity world")
    {
        // Batch create entities and components
        // that we don't particularly care about.
        int amount = ::iterations;
        for(int i = 0; i < (amount - 1); i++)
        {
            components.create(entities.create(), type);
        }

        // Create a final entity and component pair.
        std::shared_ptr<Entity> entity;
        std::shared_ptr<Component> component;

        entity      = entities.create();
        component   = components.create(entity, type);

        // Create
        WHEN("Creating a duplicate component")
        {
            std::shared_ptr<Component> fresh_component;
            fresh_component = components.create(entity, type);

            THEN("The original component is untouched")
            {
                REQUIRE(fresh_component == component);            
            }
        }

        // Get
        WHEN("Finding a component by ID")
        {
            std::shared_ptr<Component> found_component;
            found_component = components.get(component->id);

            THEN("The component is found")
            {
                REQUIRE(found_component == component);            
            }
        }

        WHEN("Finding a component by an ID that does not exist")
        {
            std::shared_ptr<Component> found_component;
            found_component = components.get(-1);

            THEN("The component is null")
            {
                REQUIRE(found_component == nullptr);          
            }
        }

        WHEN("Finding a component by entity and type")
        {
            std::shared_ptr<Component> found_component;
            found_component = components.get(entity, type);

            THEN("The component is found")
            {
                REQUIRE(found_component == component);            
            }
        }

        WHEN("Getting all components for an entity")
        {
            std::set<std::shared_ptr<Component>> found_component_set;
            found_component_set = components.get(entity);

            THEN("The amount of components is correct")
            {
                REQUIRE(found_component_set.size() == 1);
            }
        }

        WHEN("Getting all components for an invalid entity")
        {
            // Create dud entity.
            std::shared_ptr<Entity> fake_entity;
            fake_entity = nullptr;

            std::set<std::shared_ptr<Component>> found_component_set;
            found_component_set = components.get(fake_entity);

            THEN("No components are returned")
            {
                REQUIRE(found_component_set.size() == 0);
            }
        }

        WHEN("Getting all components for a component type")
        {
            std::set<std::shared_ptr<Component>> found_component_set;
            found_component_set = components.get(type);

            THEN("The amount of components is correct")
            {
                REQUIRE(found_component_set.size() == amount);
            }
        }

        WHEN("Getting all components for an invalid component type")
        {
            // Create dud type.
            std::shared_ptr<ComponentType> fake_type;
            fake_type = nullptr;

            std::set<std::shared_ptr<Component>> found_component_set;
            found_component_set = components.get(fake_type);

            THEN("No components are returned")
            {
                REQUIRE(found_component_set.size() == 0);
            }
        }

        // Delete
        WHEN("Deleting a component by ID")
        {
            size_t before;
            ptrdiff_t diff;

            before = components.size();
            components.remove(component->id);

            THEN("The amount of components is smaller by one")
            {
				diff = (components.size() - before);
                REQUIRE(diff == -1);
            }
        }

        WHEN("Deleting a component by pointer")
        {
            size_t before;
            ptrdiff_t diff;

            before = components.size();
            components.remove(component);

            THEN("The amount of components is smaller by one")
            {
				diff = (components.size() - before);
                REQUIRE(diff == -1);
            }
        }

        WHEN("Deleting a component that does not exist")
        {
            std::shared_ptr<Component> fake_component;
            fake_component = nullptr;

            size_t before;
            ptrdiff_t diff;

            before = components.size();
            components.remove(fake_component);

            THEN("The amount of components does not change")
            {
				diff = (components.size() - before);
                REQUIRE(diff == 0);
            }
        }
    }
}