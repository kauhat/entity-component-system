#include "test.h"

using namespace ECS;

namespace Components
{
	// Destructor
	Test::~Test()
	{}

	// Factory
	Component_Shared Test::create()
	{
	    return std::make_shared<Test>();
	}

	// Type
	ComponentType Test::getType()
	{
	    return ComponentTypeBuilder().name("Test").factory(create).build();
	}
}