#pragma once
#include <core/serialization.h>
#include <ecs/entity.h>
#include <ecs/component.h>
#include <ecs/component_type.h>
#include <string>

namespace Components
{
    struct Test : public ECS::Component
    {
        ~Test() override;

        // Properties
        bool touched;

        // Factory
        static std::shared_ptr<ECS::Component> create();
        static ECS::ComponentType getType();
    };
}

/*
 * Cereal
 */
namespace cereal
{
    template <class Archive>
    void serialize(Archive& ar, Components::Test& com)
    {
        ar(
            cereal::virtual_base_class<ECS::Component>(&com),
            cereal::make_nvp("touched", com.touched));
    }
}

CEREAL_REGISTER_TYPE(Components::Test);