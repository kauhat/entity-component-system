#include "catch.hpp"
#include "common.h"

#include "components/test.h"
#include "systems/testing.h"

#include <ecs/ecs.h>


SCENARIO("Systems can be activated and processed", "[system manager]")
{
	// Setup world
    std::shared_ptr<World> world = WorldBuilder().build();

    // Grab useful objects.
    EntityManager&      entities    = world->entity_manager;
    ComponentManager&   components  = world->component_manager;
    SystemManager&   	systems  	= world->system_manager;

    // Register a component type.
    components.types.registerType(Components::Test::getType());

	std::shared_ptr<ComponentType> type;
    type = components.types.getType("Test");

    GIVEN("A populated entity world")
    {
    	// Batch create components.
        int amount = ::iterations;
        for(int i = 0; i < amount - 1; i++)
        {
            components.create(entities.create(), type);
        }

		std::shared_ptr<Component> component;
		component = components.create(entities.create(), type);

    	WHEN("A test system is added")
    	{
    		size_t before;
			ptrdiff_t diff;

        	before = systems.size();
	    	systems.add(std::make_shared<Systems::Testing>());

	    	THEN("The amount of running systems has increased by one")
	    	{
	    		diff = (systems.size() - before);
	    		REQUIRE(diff == 1);
	    	}

			AND_WHEN("Systems have not yet been processed")
			{
				THEN("Predictable results of the system being run cannot be found")
				{
					auto test = std::static_pointer_cast<Components::Test>(component);
					REQUIRE(!test->touched);
				}
			}

	    	AND_WHEN("Systems are processed")
	    	{
	    		systems.process();

	    		THEN("Predictable results of the system being run can be found")
	    		{
					auto test = std::static_pointer_cast<Components::Test>(component);
					REQUIRE(test->touched);
	    		}
	    	}
    	}
    }
}