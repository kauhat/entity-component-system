#pragma once
#include <ecs/entity_system.h>
#include <core/serialization.h>

using namespace ECS;

namespace Systems
{
	class Testing : public ECS::EntitySystem
	{
	public:
	    ~Testing() override {};

	    Aspect_Shared initializeAspect() override;
	    void processSystem();
	};
}