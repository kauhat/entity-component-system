#include "testing.h"
#include "ecs/components/test.h"

#include <ecs/ecs.h>
#include <unordered_map>

using namespace ECS;

namespace Systems
{
    Aspect_Shared Testing::initializeAspect()
    {
        // Grab useful objects.
        auto world = world_.lock();
        auto& components = world->component_manager;

        auto type = components.types.getType("Test");

        // Create new aspect object.
        AspectInterest interest;
        interest.any = {};

        std::shared_ptr<Aspect> aspect;
        aspect = std::make_shared<Aspect>();
        aspect->setWorld(world);
        aspect->setInterest(interest);

        return aspect;
    }

    void Testing::processSystem()
    {
        // Grab useful objects.
        auto world = world_.lock();
        auto& components = world->component_manager;

        auto type = components.types.getType("Test");

        //
        auto entities = getEntities();
        for(auto& entity : entities)
        {
            std::shared_ptr<Component> component;
            std::shared_ptr<Components::Test> test;

            component = components.get(entity, type);
            test = std::static_pointer_cast<Components::Test>(component);

            if(!test->touched)
            {
                test->touched = true;
            }
        }
    }
}