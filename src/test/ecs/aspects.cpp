#include "catch.hpp"
#include "common.h"

#include "components/test.h"

#include <ecs/ecs.h>

using namespace ECS;

SCENARIO("Aspects can be created and removed", "[aspect subscription manager]")
{
    // Setup world
    std::shared_ptr<World> world = WorldBuilder().build();

    // Grab useful objects.
    EntityManager&      entities    = world->entity_manager;
    ComponentManager&   components  = world->component_manager;
    AspectSubject&      aspects     = world->aspect_subject;

    // Register a component type.
    components.types.registerType(Components::Test::getType());

    std::shared_ptr<ComponentType> type;
    type = components.types.getType("Test");

    GIVEN("An empty entity world")
    {
        AspectInterest interest;
        interest.any = {type};

        std::shared_ptr<Aspect> aspect;
        aspect = std::make_shared<Aspect>();
        aspect->setWorld(world);
        aspect->setInterest(interest);
        
        WHEN("No relevant entities are added")
        {
            auto entity = entities.create();

            THEN("The aspect matches no entities")
            {
                auto found_entities = aspect->getEntities();
                REQUIRE(found_entities.size() == 0);
            }
        }

        WHEN("A relevant entity is added")
        {
            auto entity = entities.create();
            components.create(entity, type);

            THEN("The aspect matches a single entity")
            {
                auto found_entities = aspect->getEntities();
                REQUIRE(found_entities.size() == 1);
            }
    
            AND_WHEN("The entity is deleted")
            {
                entities.remove(entity);

                THEN("The aspect matches no entities")
                {
                    auto found_entities = aspect->getEntities();
                    REQUIRE(found_entities.size() == 0);
                }
            }
        }

        WHEN("Multiple entities are added")
        {
            // Batch create entities and components
            // that we don't particularly care about.
            int amount = ::iterations;
            for(int i = 0; i < amount; i++)
            {
                components.create(entities.create(), type);
            }

            THEN("The aspect result size matches the amount of new entities")
            {
                auto found_entities = aspect->getEntities();
                REQUIRE(found_entities.size() == amount);
            }
        }
    }
}