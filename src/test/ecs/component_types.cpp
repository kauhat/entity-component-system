#include "catch.hpp"
#include "common.h"

#include "components/test.h"

#include <ecs/ecs.h>

using namespace ECS;

SCENARIO("Component types be registered and unregistered", "[component type manager]")
{
    // Setup world
    std::shared_ptr<World> world = WorldBuilder().build();

    // Grab useful objects.
    ComponentManager& components = world->component_manager;

    GIVEN("An entity world with no component types registered")
    {
        WHEN("Attempting to get an unregistered type")
        {
            std::shared_ptr<ComponentType> found_type;
            found_type = components.types.getType("Unregistered Type");

            THEN("Returned pointer is null")
            {
                REQUIRE(found_type == nullptr);
            }
        }

        WHEN("Registering a component type")
        {
            // Get type information
            ComponentType type = Components::Test::getType();
            components.types.registerType(type);

            THEN("Can find component type by name")
            {
                // Get the registered type.
                std::shared_ptr<ComponentType> found_type;
                found_type = components.types.getType(type.name);

                REQUIRE(found_type->name    == type.name);
                REQUIRE(found_type->factory == type.factory);
            }
        }
    }

    GIVEN("An entity world with component types registered")
    {
        ComponentType type = Components::Test::getType();
        components.types.registerType(type);

        WHEN("Attempting to unregistered a type")
        {
            components.types.unregisterType(type.name);

            THEN("The type is no longer used")
            {
                std::shared_ptr<ComponentType> found_type;
                found_type = components.types.getType(type.name);


                REQUIRE(found_type == nullptr);
            }
        }

        WHEN("Attempting to re-register an existing type")
        {
            std::shared_ptr<ComponentType> fresh_type, existing_type;
            std::string type_name, alt_type_name;

            type_name           = type.name;
            alt_type_name       = "I'm legit!";

            // Get the existing type.
            existing_type       = components.types.getType(type_name);

            // Change the type in a way we can track.
            existing_type->name = alt_type_name;

            // Register the type again.
            components.types.registerType(Components::Test::getType());

            THEN("The existing type is used")
            {
                fresh_type = components.types.getType(type_name);

                // Returned type should be the same object, so our changes will keep.
                REQUIRE(fresh_type->name == alt_type_name);
            }
        }
    }
}