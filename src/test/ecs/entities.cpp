#include "catch.hpp"
#include "common.h"

#include <ecs/ecs.h>

using namespace ECS;

SCENARIO("Entities can be created and removed", "[entity manager]")
{
    // Setup world
    std::shared_ptr<World> world = WorldBuilder().build();

    // Grab useful objects.
    EntityManager& entities = world->entity_manager;

    GIVEN("An empty entity world")
    {
        WHEN("Creating an entity")
        {
            // Create entity.
            std::shared_ptr<Entity> entity;
            entity = entities.create();

            THEN("The entity exists")
            {
                REQUIRE(entity != nullptr);
                REQUIRE(entity->world.lock() == world);
            }
        }

        WHEN("Creating multiple entities")
        {
            size_t before, after;
            before = entities.size();

            // Batch create components.
            int amount = ::iterations;
            for(int i = 0; i < amount; i++)
            {
                entities.create();
            }

            after = entities.size();

            THEN("The amount of entities has increased the amount created")
            {
				ptrdiff_t diff = (after - before);
                REQUIRE(diff == amount);
            }
        }
    }

    GIVEN("A populated entity world")
    {
        // Batch create entities that we don't particularly care about.
        int amount = ::iterations;
        for(int i = 0; i < (amount - 1); i++)
        {
            entities.create();
        }

        // Create a final entity.
        std::shared_ptr<Entity> entity;
        entity = entities.create();
    
        // Get
        WHEN("Finding an entity by ID")
        {
            std::shared_ptr<Entity> found_entity;
            found_entity = entities.get(entity->id);

            THEN("The entity is found")
            {
                REQUIRE(found_entity == entity);            
            }
        }

        WHEN("Finding an entity by an ID that does not exist")
        {
            std::shared_ptr<Entity> found_entity;
            found_entity = entities.get(-1);

            THEN("The entity is null")
            {
                REQUIRE(found_entity == nullptr);            
            }
        }

        // Delete
        WHEN("Deleting a entity by ID")
        {
            size_t before, after;

            before = entities.size();
            entities.remove(entity->id);
            after = entities.size();

            THEN("The amount of entities is smaller by one")
            {
				ptrdiff_t diff = (after - before);
                REQUIRE(diff == -1);
            }
        }

        WHEN("Deleting a entity by pointer")
        {
            size_t before, after;

            before = entities.size();
            entities.remove(entity);
            after = entities.size();

            THEN("The amount of entities is smaller by one")
            {
				ptrdiff_t diff = (after - before);
                REQUIRE(diff == -1);
            }
        }

        WHEN("Deleting a entity that does not exist")
        {
            size_t before, after;

            before = entities.size();
            entities.remove(nullptr);
            after = entities.size();

            THEN("The amount of entities does not change")
            {
				ptrdiff_t diff = (before - after);
                REQUIRE(diff == 0);
            }
        }
    }
}