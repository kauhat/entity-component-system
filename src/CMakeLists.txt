cmake_minimum_required(VERSION 2.8)
cmake_policy(SET CMP0017 NEW)

set_property(GLOBAL PROPERTY USE_FOLDERS ON)
set(CMAKE_CONFIGURATION_TYPES Debug Release)

# Get the compiling platform
if("${CMAKE_SYSTEM_NAME}" STREQUAL "Linux")
    set(OS_LINUX 1)
    set(OS_POSIX 1)

elseif("${CMAKE_SYSTEM_NAME}" STREQUAL "Darwin")
    set(OS_MACOSX 1)
    set(OS_POSIX 1)

elseif("${CMAKE_SYSTEM_NAME}" STREQUAL "Windows")
    set(OS_WINDOWS 1)
    set(OS_POSIX 0)

endif()

#
project(Engine)
option(ECS_TESTING "Run testing." TRUE)

if(ECS_TESTING)
    add_definitions(-DECS_TESTING)
endif()

# Global compiler flags
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS}")
add_definitions(-std=c++11)

# Platform specific arguments
if(OS_LINUX)
    # Linux compiler arguments
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS}"
        "-fPIC -fvisibility=hidden")

    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS}"
        " -Wall -Werror -Wno-missing-field-initializers -Wno-unused-parameter")

elseif(OS_MACOSX)
    # Mac compiler

elseif(OS_WINDOWS)
    # Windows compiler arguments

endif()

# Output directories.
#set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${PROJECT_BINARY_DIR}/bin)
#set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${PROJECT_BINARY_DIR}/bin)
#set(EXECUTABLE_OUTPUT_PATH         ${PROJECT_BINARY_DIR}/bin)

# Paths
set(SHARED_ROOT ${CMAKE_SOURCE_DIR}/lib)
set(EXT_PROJECTS_DIR ${CMAKE_SOURCE_DIR}/external)

# External dependencies.
add_subdirectory(${EXT_PROJECTS_DIR}/catch)
include_directories(${CATCH_INCLUDE_DIR} ${COMMON_INCLUDES})

add_subdirectory(${EXT_PROJECTS_DIR}/cereal)
include_directories(${CEREAL_INCLUDE_DIR} ${COMMON_INCLUDES})

# Main Project
add_subdirectory(main)
add_subdirectory(${SHARED_ROOT}/ascii)
add_subdirectory(${SHARED_ROOT}/console)
add_subdirectory(${SHARED_ROOT}/core)
add_subdirectory(${SHARED_ROOT}/data)
add_subdirectory(${SHARED_ROOT}/ecs)
add_subdirectory(${SHARED_ROOT}/generic_system)
add_subdirectory(${SHARED_ROOT}/utility)

# Unit Testing
enable_testing(true)
add_subdirectory(test)

# Print config.
message(STATUS "Generator:              ${CMAKE_GENERATOR}")
message(STATUS "Platform:               ${CMAKE_SYSTEM_NAME}")
message(STATUS "Build Type:             ${CMAKE_BUILD_TYPE}")
