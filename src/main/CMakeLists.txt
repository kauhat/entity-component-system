add_definitions(-DBUILDING_DYNAMIC_LIBRARY)

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)

set(INCLUDE_DIR ${CMAKE_CURRENT_SOURCE_DIR})
set(SOURCE_DIR  ${CMAKE_CURRENT_SOURCE_DIR})

# Sources.
set(SOURCE_FILES
    ${SOURCE_FILES}
    ${SOURCE_DIR}/main.cpp
    ${SOURCE_DIR}/commands/core.cpp
    ${SOURCE_DIR}/commands/file.cpp)

# Test subdirectory.
# if(ECS_TESTING)
# 	message(STATUS "Testing...")
#     add_definitions(-DECS_TESTING)
#     add_subdirectory(test)
# endif()

#
add_executable(Engine ${SOURCE_FILES})

# Libraries
find_package(Threads)

# Library Headers.
target_include_directories(Engine PUBLIC
    "${VENDOR_ROOT}/cereal/include"
    GenericSystem
    EngineCore
    Console
    EntityComponentSystem
    ${INCLUDE_DIR})

# Linker.
target_link_libraries(Engine
    GenericSystem
    EngineCore
    Console
    EntityComponentSystem
    ${CMAKE_THREAD_LIBS_INIT})