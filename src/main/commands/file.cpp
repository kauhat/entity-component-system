#include "commands.h"

#include <core/common.h>
#include <core/service.h>
#include <ecs/component.h>
#include <ecs/component_manager.h>
#include <generic_system/filesystem.h>

#include <fstream>
#include <iostream>
#include <vector>

namespace CLI
{
    void save(Context context)
    {
        std::string     path = "out.cereal";
        std::ofstream   os(path, std::ios::binary);

        if (os)
        {
            auto weak_cm = Core::Locator::getService<ECS::ComponentManager>();
            std::shared_ptr<ECS::ComponentManager> cm = weak_cm.lock();

            if (!cm)
            {
                std::cerr
                << "Could not access Component Manager service."
                << std::endl;

                return;
            }

            cereal::BinaryOutputArchive archive(os);

            // Get position in stream before writing.
            std::streamoff posBefore, posAfter;
            posBefore = os.tellp();

            // Write to stream.
            //archive(cereal::make_nvp("components", cm->components));

            // Get position in stream after writing.
            posAfter = os.tellp();

            std::cout << "Output file size: " << (posAfter - posBefore) << " bytes." << "\n";
        }
        else
        {
            std::cout << "File \"" << path << "\" does not exist." << "\n";
        }
    }

    void load(Context context)
    {
        std::string     path = "out.cereal";
        std::ifstream   is(path, std::ios::binary);

        if (is)
        {
            auto weak_cm = Core::Locator::getService<ECS::ComponentManager>();
            std::shared_ptr<ECS::ComponentManager> cm;

            if (!(cm = weak_cm.lock()))
            {
                std::cerr
                << "Could not access Component Manager service."
                << std::endl;

                return;
            }

            cereal::BinaryInputArchive archive(is);
            //archive(cereal::make_nvp("components", cm->components));
        }
        else
        {
            std::cout << "File \"" << path << "\" does not exist." << "\n";
        }
    }

    std::vector<std::shared_ptr<Command>> fileCommandsFactory()
    {
        std::shared_ptr<Command> root = CommandBuilder()
            .name("file")
            .build();

        std::shared_ptr<Command> save = CommandBuilder()
            .name("save")
            .sypnosis("Saves program state to file.")
            .description("")
            .functionPointer(CLI::save)
            .build();

        std::shared_ptr<Command> load = CommandBuilder()
            .name("load")
            .sypnosis("Loads the program state from a file.")
            .description("")
            .functionPointer(CLI::load)
            .build();

        root->node->appendChild(save->node);
        root->node->appendChild(load->node);

        return {root, save, load};
    }
}
