#pragma once
#include <core/common.h>
#include <console/console.h>
#include <console/command.h>
#include <vector>

namespace CLI
{
	std::vector<std::shared_ptr<CLI::Command>> coreCommandsFactory();
	std::vector<std::shared_ptr<CLI::Command>> fileCommandsFactory();
}