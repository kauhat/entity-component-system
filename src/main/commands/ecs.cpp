#include "ecs.h"

namespace Commands
{
    void create(::CLI::Context context)
    {
        auto    weak_cm = Core::Locator::getService<ECS::ComponentManager>();
        auto    cm = weak_cm.lock();

        if (!cm)
        {
            return;
        }

        //

        if (context.args.arguments.size() > 0)
        {
            std::map<std::string, std::string>& options = context.args.options;
            int size = 1;

            // Get search argument
            auto search = options.find("size");
            if (search != context.args.options.end())
            {
                int in_size = std::stoi(search->second);

                if (in_size > 1)
                {
                    size = in_size;
                    std::cout << "Creating " << size << " objects." << "\n";
                }
            }

            for (int i=0; i < size; i++)
            {
                //cm->create(context.args.arguments[0]);
            }
        }

        std::cout << std::endl;
    }

    void debug(::CLI::Context context)
    {
        auto    weak_cm = Core::Locator::getService<ECS::ComponentManager>();
        auto    cm = weak_cm.lock();

        if (!cm)
        {
            return;
        }

        //

        /*for(const Component_Shared& component : cm->components)
           {
            std::cout   << "Component: "
                        << Utility::sethex << unsigned(component->id) << Utility::reset
                        << "\n";

            cereal::JSONOutputArchive archive(std::cout);
            archive(component);

            std::cout << "\n";
           }*/

        std::cout << std::endl;
    }
}

void ComponentManager::registerConsoleCommands()
{
    static ::CLI::Command cm_base, cm_create, cm_debug;

    cm_base.name = "component";
    cm_base.AppendChild(cm_create);
    cm_base.AppendChild(cm_debug);

    cm_create.fp    = Commands::create;
    cm_create.name  = "create";
    cm_create.sypnosis      = "Create a component.";
    cm_create.description   = "";

    cm_debug.fp     = Commands::debug;
    cm_debug.name   = "debug";
    cm_debug.sypnosis = "Debug a component.";
    cm_debug.description =  "";

    ::CLI::Console::commands.insert({
                                        {cm_base.name, cm_base}
                                    });
}
}