#include "commands.h"

#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>
#include <memory>


namespace CLI
{
    void quit(Context context)
    {
        std::cout << "Quitting..." << std::endl;

        // Todo: Unsafe.
        std::exit(EXIT_SUCCESS);
    }

    void help(Context context)
    {
        std::cout   << "For more information about a specific command, "
                    << "type \"help command\"." << "\n";

        std::cout << "Listing available commands..." << "\n";

        auto search = context.console.root->findChild(context.args.arguments);
        for (auto& command_node : search.deepest->node->getChildren())
        {
            std::shared_ptr<Command> command = command_node->owner.lock();

            if(command)
            {
                std::cout   << command->name << "\n"
                            << std::string(context.console.INDENT_SIZE, ' ')
                            << command->sypnosis << "\n";
            }
        }

        std::cout << "\n" << std::flush;
    }

    std::vector<std::shared_ptr<Command>> coreCommandsFactory()
    {
        std::shared_ptr<Command> quit = CommandBuilder()
            .name("quit")
            .functionPointer(CLI::quit)
            .build();

        std::shared_ptr<Command> help = CommandBuilder()
            .name("help")
            .functionPointer(CLI::help)
            .build();

        return {quit, help};
    }
}