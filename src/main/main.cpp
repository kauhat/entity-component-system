#include "core/core.h"
#include "console/console.h"
#include "commands/commands.h"

#include <iostream>
#include <generic_system/filesystem.h>

int main(int argc, char* argv[])
{
    //
    std::cout.sync_with_stdio(false);

    //
    CLI::Console console = CLI::Console();
    console.registerCommands(CLI::coreCommandsFactory());
    console.registerCommands(CLI::fileCommandsFactory());
    console.init();

    return 0;
}