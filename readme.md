# Read Me
Hi there, thanks for looking at this project. It's a basic entity component system framework!

To build this project, you'll need to have the following programs installed.

* Git
* CMake
* A supported C++14 compiler

## Building
If you're familiar with CMake, you can probably jump right in.

The external projects are a bit temperamental, so if they fail at right away it might be worth re-running CMake and compile again. If after that the external projects are failing to download, check that the Git paths are up to date under src/external.

Find out which generator you need to use for your IDE from [the CMake documentation](https://cmake.org/cmake/help/v3.0/manual/cmake-generators.7.html).

1. Starting here, create a new directory named `build`.
2. `cd` inside the new directory.
3. Run `cmake ../src -G "your generator goes here"` and let it do it's thing.
4.	* If building from an IDE: Open the generated project file.
	* If building from the command line: Run `make` to compile.
5. You're all set!

Multiple build targets are present. The `Engine` executable is a bare bones command line application with a few mostly useless commands.

The `Test` executable will run all implemented tests, and is probably the best example of the ECS in it's current state. The `Test` executable uses the [Catch](https://github.com/philsquared/Catch) unit testing framework, so you may notice a lack of `main` function. This is legit. If a test fails you'll certainly know about it, and it was probably my fault.

## Todo
Plenty of things on this list...

* Make a nice example of the framework in use.
* Entity Component System
	* Implement world methods. (Update/Process/Delta)
	* Implement added/deleted list for aspects.
	* Consolidate entity/component change notifications.
	* Proper warning and error handling. (Currently outputs warnings to `std:cerr`, spamming console in tests etc)
	* Don't assume internal pointers are valid. (Such as `world_`)
	* Builder classes
		* Do we really need all these builders, is there a better way? They're great for initializing things in the optimal order.
		* Create a System Builder class.
* Command line front end
	* Finish implementing existing commands.
	* Implement more commands.
* Utility Libraries
	* The cross platform System library isn't cross platform. (Need to add support for Unix paths)
	* Raise proper exceptions on in various utility libraries.
	* Merge utility libraries? (A build target just for useful character codes is excessive)
* Unit tests
	* Write tests for serialization of objects.
	* Unify testing style.
* Find the perfect Uncrustify settings.